package com.example.tablist;

import com.example.tablist.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.TabList;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

/**
 *
 * 创建人：xuqing
 * 创建时间：2020年12月20日15:33:53
 * 类说明：  tablist 示例
 *
 *
 */


public class MainAbility extends Ability {
    private  String[] str={"image","Video","Audio","HuaweiShare"};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initview();
    }

    private void initview() {
        TabList tabList = (TabList) findComponentById(ResourceTable.Id_tab_list);
        if(tabList!=null){
            for (int i = 0; i < str.length; i++) {
                TabList.Tab tab = tabList.new Tab(getContext());
                tab.setText(str[i]);
                tabList.addTab(tab);
            }
           /* tabList.setTabLength(60); // 设置Tab的宽度
            tabList.setTabMargin(26); // 设置两个Tab之间的间距*/

            tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
                @Override
                public void onSelected(TabList.Tab tab) {
                    // 当某个Tab从未选中状态变为选中状态时的回调

                    new ToastDialog(MainAbility.this)
                            .setText("从未选中状态变为选中状态时的回调")
                            .setAlignment(LayoutAlignment.CENTER)
                            .show();
                }

                @Override
                public void onUnselected(TabList.Tab tab) {
                    // 当某个Tab从选中状态变为未选中状态时的回调
                    new ToastDialog(MainAbility.this)
                            .setText("从选中状态变为未选中状态时的回调")
                            .setAlignment(LayoutAlignment.CENTER)
                            .show();
                }

                @Override
                public void onReselected(TabList.Tab tab) {
                    // 当某个Tab已处于选中状态，再次被点击时的状态回调
                    new ToastDialog(MainAbility.this)
                            .setText("已处于选中状态，再次被点击时的状态回调")
                            .setAlignment(LayoutAlignment.CENTER)
                            .show();
                }
            });

        }


    }
}
